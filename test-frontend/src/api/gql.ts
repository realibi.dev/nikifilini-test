import { createClient, fetchExchange } from "urql";
console.log(process.env.API_URL);

const client = createClient({
  url: process.env.API_URL || "http://localhost:3000/graphql",
  fetchOptions: () => {
    const token = "";
    return {
      headers: { authorization: token ? `Bearer ${token}` : '' },
    };
  },
  exchanges: [fetchExchange],
});

export default client;
