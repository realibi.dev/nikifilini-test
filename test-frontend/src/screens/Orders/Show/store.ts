import { makeAutoObservable } from "mobx";
import {SingleOrder} from "~/screens/Orders/Show/types";
import client from "api/gql";
import {ORDER_QUERY} from "~/screens/Orders/Show/queries";

export default class OrdersShowStore {
  order: SingleOrder | null = null;
  id: string | null = null;
  initialized: boolean = false;
  loading: boolean = true;

  constructor() {
    makeAutoObservable(this);
  }

  setId(id: string): void {
    this.id = id;
  }

  setOrder(order: SingleOrder): void {
    this.order = order;
  }

  async loadOrder(){
    this.loading = true;
    const result = await client.query(ORDER_QUERY, { number: this.id }).toPromise();
    this.setOrder(result.data.order);
    this.loading = false;
  }

  async initialize(){
    if(!this.initialized){
      this.initialized = true;
      this.loadOrder()
    }
  }
}
