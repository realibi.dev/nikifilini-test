import React, {useEffect} from "react";
import OrdersShowStore from "./store";
import { observer } from "mobx-react-lite";
import { useParams } from "react-router-dom";
import styles from "./styles.m.styl";
import Item from "~/screens/Orders/Show/components/Item";

type ShowParams = {
  id: string;
};

const OrdersShow = observer(
  (): JSX.Element => {
    const params: ShowParams = useParams();
    const [state] = React.useState(new OrdersShowStore());

    useEffect(() => {
        if(!state.initialized){
            state.setId(params.id);
            state.loadOrder();
        }
    }, [params]);

    return (
      <div className={styles.screenWrapper}>
        <div className={styles.screen}>
            <h1>Товары этого заказа:</h1>

            {state.loading ?
                (<p>Загрузка</p>) : (
                    <div className={styles.items}>
                        { state.order && state.order.items.length > 0 ?
                            state.order?.items.map(orderItem => (
                                    <Item item={orderItem} />
                                )) :
                            <p>Товаров нет</p>
                        }

                    </div>
                )
            }

        </div>
      </div>
    );
  }
);

export default OrdersShow;
